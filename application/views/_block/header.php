	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" type="image/png" href="<?= base_url()?>publik/img/logo.png">

  <title><?= $page_title ?> | SIMANTAP</title>

  <!-- Custom fonts for this template -->
  <link href="<?= base_url() ?>publik/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Bree+Serif&family=Fugaz+One&family=Righteous&display=swap" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?= base_url() ?>publik/css/sb-admin-2.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="<?= base_url() ?>publik/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">